package com.company;

class Point3d {
    double x;
    double y;
    double z;

    public Point3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double distance(double x, double y, double z) {
        return Math.sqrt(Math.pow(this.x - x,2)+Math.pow(this.y-y,2)+Math.pow(this.z-z,2));
    }
}

public class Main {
    public static void main(String[] args) {
        Point3d earth = new Point3d(20.3, 100.3, 109);
        System.out.println(earth.distance(0,0,0));
    }
}
